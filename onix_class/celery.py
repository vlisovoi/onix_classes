# -*- coding: utf-8 -*-
import os
from celery import Celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'onix_class.settings')
app = Celery('onix_class')
app.config_from_object('django.conf:settings')
app.autodiscover_tasks()

