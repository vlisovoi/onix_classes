from django.contrib import admin
from django.urls import path, include
from .drf_url import urlpatterns as doc_drf

urlpatterns = [
    path('admin/', admin.site.urls, name='admin'),
    path('dashboard/', include('teach_admin.urls')),
    path('api/', include('teach_api.urls', namespace='api')),
    path('auth/', include('web_auth.urls')),
]
urlpatterns += doc_drf