from django.urls import path, include
from . import views


urlpatterns = [
    path('', include('django.contrib.auth.urls')),
    path('', include('allauth.urls')),
    path('register/', views.register, name='register_view'),


]