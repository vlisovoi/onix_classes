from django.shortcuts import render, redirect
from django.contrib.auth import get_user_model
from .forms import RegistrationForm

# Create your views here.


def register(request):
    if request.method == "POST":
        User = get_user_model()
        form = RegistrationForm(request.POST)
        if form.is_valid():
            user = User.objects.create_user(form['email'].value(), form['first_name'].value(), form['last_name'].value(), form['password'].value())
            return redirect('/')
    else:
        form = RegistrationForm()

    return render(request, 'registration/registration.html', {
        'form': form,
    })