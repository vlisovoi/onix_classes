from django.db import models
from django.contrib.auth.models import User
from django.core.validators import MinValueValidator, MaxValueValidator
from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.base_user import AbstractBaseUser
from django.utils.translation import ugettext_lazy as _
from .managers import TeacherManager
from django.conf import settings
from django.utils import timezone


class TeacherUser(AbstractBaseUser, PermissionsMixin):
    first_name = models.CharField(max_length=100, null=False, blank=True)
    last_name = models.CharField(max_length=100, null=False, blank=True)
    email = models.EmailField(_('email address'), unique=True)
    avatar = models.ImageField(upload_to='avatars/%Y/', blank=True)
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    date_joined = models.DateTimeField(default=timezone.now)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = TeacherManager()

    def str(self):
        return self.email



class Class(models.Model):
    class Meta:
        verbose_name_plural = 'Classes'

    name = models.CharField(max_length=100)
    creator = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='classes')
    send_mail = models.BooleanField()
    allowed_teachers = models.ManyToManyField(settings.AUTH_USER_MODEL, related_name='allowed_teachers', blank=True)

    def __str__(self):
        return self.name


class Student(models.Model):
    name = models.CharField(max_length=100)
    # 'form' changed to 'class_am', where 'am' stands for alma mater
    class_am = models.ForeignKey(Class, on_delete=models.CASCADE, related_name='students')

    def __str__(self):
        return f"{self.name}, {self.class_am}"


class Activity(models.Model):
    class Meta:
        verbose_name_plural = 'Activities'

    name = models.CharField(max_length=100)
    teacher = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='activities')

    def __str__(self):
        return self.name


class Assessment(models.Model):
    activity = models.ForeignKey(Activity, on_delete=models.CASCADE, related_name='activity')
    student = models.ManyToManyField(Student, related_name='assessments')
    mark = models.IntegerField(default=0, validators=[MinValueValidator(1), MaxValueValidator(12)])
    date = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return 'Student: ' + '; '.join(map(str, self.student.all())) + f' | Mark: {self.mark} | Date: {self.date:%d-%m-%Y %H:%M}' 


class Report(models.Model):
    
    title = models.CharField(max_length=100)
    report_type = models.CharField(max_length=100, choices=[['fb', 'feedback'], ['bg', 'bug']])
    creator = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    content = models.TextField()

    def __str__(self):
        return f'Title: {self.title}, type: {self.report_type}' 
