# -*- coding: utf-8 -*-
from celery.task import periodic_task
from django.core.mail import send_mail
from teach_admin.models import Class, Student
from celery.schedules import crontab


# for the test
# @periodic_task(run_every=(crontab(minute="*/1")))

# celery -A onix_class worker -l info
# celery -A onix_class flower
# celery -A onix_class beat -l info



@periodic_task(run_every=(crontab(0, 0, day_of_month='1')))
def periodic_task():
    """Periodic task that will send the teacher
     a report on all classes for which he wants to receive a report"""
    id_teacher_creator_class = []
    for groups in Class.objects.filter(send_mail=True):
        if groups.creator_id not in id_teacher_creator_class:
            mail = groups.creator
            message = ''
            id_teacher_creator_class.append(groups.creator_id)
            for student in Student.objects.filter(class_am__creator_id=groups.creator_id).order_by('class_am_id'):
                message += f'{student.name} {student.class_am}, \n'
            subject = 'Report for all classes'
            send_mail(subject,
                      message,
                      'admin@admin.com',
                      [mail], html_message=f'<hl><strong>{subject}</strong>!</hl><br> <p>{message}</p>')
