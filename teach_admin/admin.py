from django.contrib import admin
from .models import TeacherUser as Teacher, Class, Student, Activity, Assessment, Report


# Register your models here.
admin.site.register(Teacher)
admin.site.register(Class)
admin.site.register(Student)
admin.site.register(Activity)
admin.site.register(Assessment)
admin.site.register(Report)
