from django import forms
from .models import TeacherUser, Class


class TeacherEditForm(forms.ModelForm):
    class Meta:
        model = TeacherUser
        exclude = ['groups', 'user_permissions']


class MailForm(forms.Form):
    subject = forms.CharField(label='Subject', max_length=100)
    message = forms.CharField(widget=forms.Textarea)


class ClassEditForm(forms.ModelForm):
    class Meta:
        model = Class
        fields = '__all__'
