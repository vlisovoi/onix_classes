from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required
from django.core.mail import send_mail
from django.http import HttpResponse
from .models import TeacherUser, Class, Student, Report
from datetime import date, datetime, timedelta
from .forms import MailForm, TeacherEditForm, ClassEditForm
import pytz


@login_required
def index(request, template="dashboard/dashboard.html"):
    teachers_list = TeacherUser.objects.all()
    classes_list = Class.objects.all()
    teacher_statistics = []
    for date_v in range(0, 71, 7):
        date_start = date.today() - timedelta(days=date_v)
        datetime_start = datetime(year=date_start.year, month=date_start.month, day=date_start.day, tzinfo=pytz.UTC)
        date_end = date.today() - timedelta(days=date_v + 7)
        datetime_end = datetime(year=date_end.year, month=date_end.month, day=date_end.day, tzinfo=pytz.UTC)
        teachers_count = teachers_list.filter(date_joined__range=[datetime_end, datetime_start]).count()
        teacher_statistics.append({
            "start_date": date_start.strftime("%m-%d"),
            "end_date": date_end.strftime("%m-%d"),
            "count": teachers_count
        })
    content = {
        'teacher_statistics': teacher_statistics,
        'all_teachers': teachers_list.count(),
        'all_classes': classes_list.count(),
    }
    return render(request, template, content)


@login_required
def teachers_page(request, template="dashboard/teachers_list.html"):
    teachers_list = TeacherUser.objects.all()
    content = {
        "all_teachers": teachers_list
    }
    return render(request, template, content)


@login_required
def teacher_block(request, teacher_id):
    teacher = get_object_or_404(TeacherUser, id=teacher_id)
    teacher.is_active = False
    teacher.save()
    return redirect('teachers_page')


@login_required
def teacher_unblock(request, teacher_id):
    teacher = get_object_or_404(TeacherUser, id=teacher_id)
    teacher.is_active = True
    teacher.save()
    return redirect('teachers_page')


@login_required
def teacher_edit(request, teacher_id):
    teacher = get_object_or_404(TeacherUser, id=teacher_id)
    if request.method == 'POST':
        form = TeacherEditForm(request.POST, request.FILES, instance=teacher)
        if form.is_valid():
            teacher = form.save()
            return redirect('teacher_edit', teacher_id=teacher.id)
    else:
        form = TeacherEditForm(instance=teacher)
    return render(request, 'dashboard/edit_teacher.html', {'form': form})


@login_required
def mail_to_teacher(request, teacher_id):
    teacher = get_object_or_404(TeacherUser, id=teacher_id)
    if request.method == 'POST':
        form = MailForm(request.POST)
        if form.is_valid():
            send_mail(
                request.POST['subject'],
                request.POST['message'],
                "admin@onixgeneral.com",
                [teacher.email])
            return redirect('teachers_page')
    else:
        form = MailForm()
    return render(request, 'dashboard/send_mail.html', {'form': form})


@login_required
def classes_page(request, template="dashboard/classes_list.html"):
    classes_list = Class.objects.all()
    students = Student.objects.all()
    all_class = []
    for class_one in classes_list:
        all_class.append({
            "id": class_one.id,
            "name": class_one.name,
            "creator": class_one.creator,
            "count_students": students.filter(class_am=class_one).count()
        })
    content = {
        "classes": all_class
    }
    return render(request, template, content)


@login_required
def class_edit(request, class_id):
    class_v = get_object_or_404(Class, id=class_id)
    student_list = Student.objects.filter(class_am=class_v)
    if request.method == 'POST':
        form = ClassEditForm(request.POST, instance=class_v)
        if form.is_valid():
            class_v = form.save()
            return redirect('class_edit', class_id=class_v.id)
    else:
        form = ClassEditForm(instance=class_v)
    return render(request, 'dashboard/class_update.html', {'form': form, "students": student_list})


@login_required
def class_del(request, class_id):
    class_v = get_object_or_404(Class, id=class_id)
    class_v.delete()
    return redirect('classes_page')


@login_required
def reports_page(request, template="dashboard/reports_list.html"):
    reports_list = Report.objects.filter(report_type="bg")
    reports = []
    for report in reports_list:
        reports.append({
            "title": report.title,
            "creator": f"{report.creator.first_name} {report.creator.last_name}",
            "email": report.creator.email,
            "content": report.content,
        })
    content = {
        "reports": reports
    }
    return render(request, template, content)


@login_required
def feedbacks_page(request, template="dashboard/feedbacks_list.html"):
    feedbacks_list = Report.objects.filter(report_type="fd")
    feedbacks = []
    for feedback in feedbacks_list:
        feedbacks.append({
            "title": feedback.title,
            "creator": f"{feedback.creator.first_name} {feedback.creator.last_name}",
            "email": feedback.creator.email,
            "content": feedback.content,
        })
    content = {
        "feedbacks": feedbacks
    }
    return render(request, template, content)
