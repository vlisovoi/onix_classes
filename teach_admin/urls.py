from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='dashboard'),
    path('teachers/', views.teachers_page, name='teachers_page'),
    path('teachers/edit/<int:teacher_id>', views.teacher_edit, name='teacher_edit'),
    path('teachers/block/<int:teacher_id>', views.teacher_block, name='teacher_block'),
    path('teachers/unblock/<int:teacher_id>', views.teacher_unblock, name='teacher_unblock'),
    path('teachers/mail/<int:teacher_id>', views.mail_to_teacher, name='mail_to_teacher'),
    path('classes/', views.classes_page, name='classes_page'),
    path('classes/edit/<int:class_id>', views.class_edit, name='class_edit'),
    path('classes/delete/<int:class_id>', views.class_del, name='class_del'),
    path('reports/', views.reports_page, name='reports_page'),
    path('feedbacks/', views.feedbacks_page, name='feedbacks_page'),
]

