from django.apps import AppConfig


class TeachAdminConfig(AppConfig):
    name = 'teach_admin'
