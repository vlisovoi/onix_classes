import io

from django.contrib.auth import get_user_model
from django.urls import reverse

from PIL import Image

from rest_framework import status
from rest_framework.test import APITestCase

from teach_admin.models import Activity, Assessment, Class, Report, Student

User = get_user_model()


class BaseTestSetUp(APITestCase):

    def setUp(self):

        self.user, created = User.objects.get_or_create(
            email='testuser@mail.com'
        )
        self.user.set_password('testpass1212')
        self.user.save()

        self.user_2, created = User.objects.get_or_create(
            email='testuser_2@mail.com'
        )
        self.user_2.set_password('testpass1212')
        self.user_2.save()

        self.user_3, created = User.objects.get_or_create(
            email='testuser_3@mail.com'
        )
        self.user_3.set_password('testpass1212')
        self.user_3.save()

        self.admin, created = User.objects.get_or_create(
            email='admin@mail.com',
            is_superuser=True,
            is_staff=True,
        )
        self.admin.set_password('testpass1212')
        self.admin.save()

        self.class_inst, created = Class.objects.get_or_create(
            name='setup',
            creator=self.user,
            send_mail=False,
        )
        self.class_inst.save()
        self.class_inst.allowed_teachers.add(self.user_2)

        self.student, created = Student.objects.get_or_create(
            name='test_student',
            class_am=self.class_inst
        )
        self.student.save()

        self.activity, created = Activity.objects.get_or_create(
            name='activity test',
            teacher=self.user
        )
        self.activity.save()

        self.assessment, created = Assessment.objects.get_or_create(
            activity=self.activity,
            mark=5,
        )
        self.assessment.student.add(self.student)
        self.assessment.save()

        self.report, created = Report.objects.get_or_create(
            title='test',
            report_type='fb',
            creator=self.user,
            content='jksahdfkjhdask'
        )
        self.report.save()

    def user_auth(self):
        url = reverse('api:token-obtain-pair')
        data = {'email': 'testuser@mail.com', 'password': 'testpass1212'}
        response = self.client.post(url, data)
        self.token = response.data['access']
        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {self.token}')
        return self.user

    def user_2_auth(self):
        url = reverse('api:token-obtain-pair')
        data = {'email': 'testuser_2@mail.com', 'password': 'testpass1212'}
        response = self.client.post(url, data)
        self.token = response.data['access']
        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {self.token}')
        return self.user_2

    def user_3_auth(self):
        url = reverse('api:token-obtain-pair')
        data = {'email': 'testuser_3@mail.com', 'password': 'testpass1212'}
        response = self.client.post(url, data)
        self.token = response.data['access']
        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {self.token}')
        return self.user_3

    def admin_auth(self):
        url = reverse('api:token-obtain-pair')
        data = {'email': 'admin@mail.com', 'password': 'testpass1212'}
        response = self.client.post(url, data)
        self.token = response.data['access']
        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {self.token}')
        return self.admin

    def generate_image(self):
        image_file = io.BytesIO()
        image = Image.new('RGBA', size=(100, 100), color=(155, 0, 0))
        image.save(image_file, format='png')
        image_file.name = 'test.png'
        image_file.seek(0)
        return image_file


class AuthTests(BaseTestSetUp):

    def test_auth_token_url(self):
        url = reverse('api:token-obtain-pair')
        data = {
            'email': self.user.email,
            'password': 'testpass1212'
        }
        response = self.client.post(url, data)
        msg = 'access token should be in response'
        self.assertIn('access', response.data, msg)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_auth_fail_url(self):
        url = reverse('api:token-obtain-pair')
        data = {
            'email': 'random@mail.com',
            'password': 'sdfgsdfdgf212'
        }
        response = self.client.post(url, data)
        # self.assertIn('access', response.data) 
        # FAIL MESSAGE: AssertionError: 'access' not found in {'detail': ErrorDetail(string='No active account found with the given credentials', code='no_active_account')}
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        msg = 'shouldnt give tokens with wrong credentials'
        self.assertNotIn('access', response.data, msg)


class ClassTests(BaseTestSetUp):

    def test_get_list_class(self):
        user = self.user_auth()
        url = reverse('api:class-list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_post_class(self):
        url = reverse('api:class-list')
        data = {
            'name': 'testclass',
            'send_mail': False
        }
        self.user_auth()
        response = self.client.post(url, data)
        post_class = Class.objects.get(pk=response.data['pk'])
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        msg = 'auth user must be class.creator by default'
        self.assertEqual(post_class.creator, self.user, msg)

    def test_post_unauth_class(self):
        url = reverse('api:class-list')
        data = {
            'name': 'testclass',
            'send_mail': False
        }
        self.client.credentials(HTTP_AUTHORIZATION='')
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_patch_class(self):
        pk = self.class_inst.pk
        url = reverse('api:class-detail', kwargs={'pk': pk})
        data_to_patch = 'testclass update'
        data = {
            'name': data_to_patch
        }
        self.user_auth()
        response = self.client.patch(url, data)
        instance = Class.objects.get(pk=pk)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(instance.name, data_to_patch, 'field must be updated')

    def test_patch_nopermission_class(self):
        pk = self.class_inst.pk
        url = reverse('api:class-detail', kwargs={'pk': pk})
        data_to_patch = 'testclass update'
        data = {
            'name': data_to_patch
        }
        self.user_2_auth()
        response = self.client.patch(url, data)
        msg = 'user which is not obj creator, have no rights to change obj'
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN, msg)

    def test_get_detail_allowed_class(self):
        pk = self.class_inst.pk
        user = self.user_2_auth()
        url = reverse('api:class-detail', kwargs={'pk': pk})
        response = self.client.get(url)
        self.assertIn(user, self.class_inst.allowed_teachers.all(), 'allowed to read teacher must be in to_read list of instance')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_detail_notallowed_class(self):
        pk = self.class_inst.pk
        user = self.user_3_auth()
        url = reverse('api:class-detail', kwargs={'pk': pk})
        response = self.client.get(url)
        self.assertNotIn(user, self.class_inst.allowed_teachers.all(), 'not allowed to read teacher should not be in to_read list of instance')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_del_class(self):
        pk = self.class_inst.pk
        self.user_auth()
        url = reverse('api:class-detail', kwargs={'pk': pk})
        response = self.client.delete(url)
        filter_obj = Class.objects.filter(pk=pk)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        msg = 'should be no objects with such pk'
        self.assertEqual(filter_obj.count(), 0, msg)

    def test_del_nopermission_class(self):
        pk = self.class_inst.pk
        self.user_2_auth()
        url = reverse('api:class-detail', kwargs={'pk': pk})
        response = self.client.delete(url)
        msg = 'User, which isnt creator should have no rights to del obj'
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN, msg)


class StudentTests(BaseTestSetUp):

    def test_get_list_student(self):
        user = self.user_auth()
        url = reverse('api:student-list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_post_student(self):
        url = reverse('api:student-list')
        class_pk = self.class_inst.pk
        data = {
            'name': 'testtesttest',
            'class_am': class_pk,
        }
        self.user_auth()
        response = self.client.post(url, data)
        instance = Student.objects.get(pk=response.data['pk'])
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        msg = 'Student class pk should be equal to self.class pk of test'
        self.assertEqual(instance.class_am.pk, class_pk, msg)

    def test_post_unauth_student(self):
        url = reverse('api:student-list')
        class_pk = self.class_inst.pk
        data = {
            'name': 'testtesttest2',
            'class_am': class_pk,
        }
        self.client.credentials(HTTP_AUTHORIZATION='')
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_get_detail_student(self):
        pk = self.student.pk
        url = reverse('api:student-detail', kwargs={'pk': pk})
        self.user_auth()
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        msg = 'name of response obj and obj from kwargs should be equal'
        self.assertEqual(response.data['name'], self.student.name, msg)

    def test_patch_student(self):
        pk = self.student.pk
        url = reverse('api:student-detail', kwargs={'pk': pk})
        data = {
            'name': 'update'
        }
        self.user_auth()
        response = self.client.get(url, data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_delete_student(self):
        pk = self.student.pk
        url = reverse('api:student-detail', kwargs={'pk': pk})
        self.user_auth()
        response = self.client.delete(url)
        filter_obj = Student.objects.filter(pk=pk)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        msg = 'should be no objects with such pk'
        self.assertEqual(filter_obj.count(), 0, msg)

    def test_delete_unauth_student(self):
        pk = self.student.pk
        url = reverse('api:student-detail', kwargs={'pk': pk})
        self.client.credentials(HTTP_AUTHORIZATION='')
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)


class ActivityTests(BaseTestSetUp):

    def test_list_activity(self):
        user = self.user_auth()
        url = reverse('api:activity-list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_post_activity(self):
        url = reverse('api:activity-list')
        data = {
            'name': 'activity test'
        }
        user = self.user_auth()
        response = self.client.post(url, data)
        instance = Activity.objects.get(pk=response.data['pk'])
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        msg = 'creator of activity must be in teacher field by default'
        self.assertEqual(instance.teacher, user, msg)

    def test_post_unauth_activity(self):
        url = reverse('api:activity-list')
        data = {
            'name': 'activity test'
        }
        self.client.credentials(HTTP_AUTHORIZATION='')
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_get_detail_activity(self):
        pk = self.activity.pk
        url = reverse('api:activity-detail', kwargs={'pk': pk})
        self.user_auth()
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        msg = 'name of response obj and obj from kwargs should be equal'
        self.assertEqual(response.data['name'], self.activity.name, msg)

    def test_patch_activity(self):
        pk = self.activity.pk
        url = reverse('api:activity-detail', kwargs={'pk': pk})
        data = {
            'name': 'update'
        }
        self.user_auth()
        response = self.client.patch(url, data)
        instance = Activity.objects.get(pk=pk)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        msg = 'instance and data fields should be equal'
        self.assertEqual(instance.name, data['name'], msg)

    def test_delete_activity(self):
        pk = self.activity.pk
        url = reverse('api:activity-detail', kwargs={'pk': pk})
        self.user_auth()
        response = self.client.delete(url)
        filter_obj = Activity.objects.filter(pk=pk)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        msg = 'should be no objects with such pk'
        self.assertEqual(filter_obj.count(), 0, msg)

    def test_delete_unauth_activity(self):
        pk = self.activity.pk
        url = reverse('api:activity-detail', kwargs={'pk': pk})
        self.client.credentials(HTTP_AUTHORIZATION='')
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)


class AssessmentTests(BaseTestSetUp):

    def test_list_assessment(self):
        user = self.user_auth()
        url = reverse('api:assessment-list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_post_assessment(self):
        url = reverse('api:assessment-list')
        data = {
            'activity': self.activity.pk,
            'student': self.student.pk,
            'mark': 4,
        }
        self.user_auth()
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_post_unauth_assessment(self):
        url = reverse('api:assessment-list')
        data = {
            'activity': self.activity.pk,
            'student': [self.student.pk, ],
            'mark': 4,
        }
        self.client.credentials(HTTP_AUTHORIZATION='')
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_get_detail_assessment(self):
        pk = self.assessment.pk
        url = reverse('api:assessment-detail', kwargs={'pk': pk})
        self.user_auth()
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['pk'], pk)

    def test_patch_assessment(self):
        pk = self.assessment.pk
        url = reverse('api:assessment-detail', kwargs={'pk': pk})
        data = {
            'mark': 1
        }
        self.user_auth()
        response = self.client.patch(url, data)
        instance = Assessment.objects.get(pk=pk)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        msg = 'instance and data fields should be equal'
        self.assertEqual(instance.mark, data['mark'], msg)

    def test_delete_assessment(self):
        pk = self.assessment.pk
        url = reverse('api:assessment-detail', kwargs={'pk': pk})
        self.user_auth()
        response = self.client.delete(url)
        filter_obj = Assessment.objects.filter(pk=pk)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        msg = 'should be no objects with such pk'
        self.assertEqual(filter_obj.count(), 0, msg)

    def test_delete_unauth_assessment(self):
        pk = self.assessment.pk
        url = reverse('api:assessment-detail', kwargs={'pk': pk})
        self.client.credentials(HTTP_AUTHORIZATION='')
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)


class ReportTests(BaseTestSetUp):

    def test_get_list_report(self):
        user = self.user_auth()
        url = reverse('api:report-list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_post_report(self):
        user = self.user_auth()
        url = reverse('api:report-list')
        data = {
            'title': 'testtitle',
            'report_type': 'fb',
            'content': 'ajkshdfkjadshfjk',
        }
        response = self.client.post(url, data)
        report_obj = Report.objects.get(pk=response.data['pk'])
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        msg = 'creator must be request user by default'
        self.assertEqual(report_obj.creator, user, msg)

    def test_post_unauth_report(self):
        url = reverse('api:report-list')
        data = {
            'title': 'testtiasdfasdfle',
            'report_type': 1,
            'content': 'ajkshdfkjadshfjk',
        }
        self.client.credentials(HTTP_AUTHORIZATION='')
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_get_detail_report(self):
        pk = self.report.pk
        url = reverse('api:report-detail', kwargs={'pk': pk})
        self.user_auth()
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_unauth_detail_report(self):
        pk = self.report.pk
        url = reverse('api:report-detail', kwargs={'pk': pk})
        self.client.credentials(HTTP_AUTHORIZATION='')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
