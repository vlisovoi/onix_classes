from django.shortcuts import get_object_or_404, get_list_or_404
from rest_framework import filters, permissions
from rest_framework.permissions import AllowAny, IsAuthenticated, IsAuthenticatedOrReadOnly
from teach_admin.models import TeacherUser as Teacher, Class, Student, Activity, Assessment, Report
from .serializers import (TeacherSerializer, StudentSerializer, ClassSerializer,
                          ActivitySerializer, AssessmentSerializer, ReportSerializer,
                          ClassCUDSerial, AssessmentCUDSerial, QuerySerializer)
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView, RetrieveAPIView, \
    RetrieveDestroyAPIView, GenericAPIView, ListAPIView
from rest_framework.response import Response
from .teach_permissions import ClassPermission
from teach_api.tasks import send_xlsx_to_email
from datetime import datetime


class TeachersView(ListCreateAPIView):
    queryset = Teacher.objects.all()
    serializer_class = TeacherSerializer
    permission_classes = [AllowAny]


class TeacherView(RetrieveUpdateDestroyAPIView):
    queryset = Teacher.objects.all()
    serializer_class = TeacherSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]


class ClassesView(ListCreateAPIView):
    queryset = Class.objects.all()
    serializer_class = ClassSerializer
    permission_classes = [IsAuthenticated]
    filter_backends = (filters.SearchFilter, filters.OrderingFilter)
    search_fields = ['creator__last_name']  # /api/classes?search=<creator_name>
    ordering_fields = ['creator__last_name']  # localhost:8000/api/classes?ordering=creator__user__username

    # Exactly 'creator__user__username'
    def get_queryset(self):
        user = self.request.user
        return Class.objects.filter(allowed_teachers=user).union(Class.objects.filter(creator=user))

    def get_serializer_class(self):
        return ClassCUDSerial if self.request.method == 'POST' else ClassSerializer


class ClassView(RetrieveUpdateDestroyAPIView):
    queryset = Class.objects.all()
    serializer_class = ClassSerializer
    permission_classes = [ClassPermission, IsAuthenticated]

    def get(self, request, *args, **kwargs):
        if self.request.query_params:
            qry_serial = QuerySerializer(data=self.request.query_params)
            qry_serial.is_valid(raise_exception=True)
            student_name = qry_serial.validated_data.get('student_name')
            class_id = self.kwargs.get('pk')
            students = Student.objects.filter(name=student_name, class_am__id=class_id)
            serializer = StudentSerializer(students, many=True)
            return Response(serializer.data)
        return self.retrieve(request, *args, **kwargs)

    def get_serializer_class(self):
        return ClassCUDSerial if self.request.method in ('PUT', 'PATCH') else ClassSerializer


class SearchStudView(RetrieveAPIView):
    queryset = Class.objects.all()
    lookup_url_kwarg = 'student_name'

    def get(self, request, *args, **kwargs):
        class_obj = self.queryset.get(id=self.kwargs.get('pk'))
        student_name = self.kwargs.get(self.lookup_url_kwarg)
        stud = get_list_or_404(Student, name=student_name, class_am=class_obj)
        serializer = StudentSerializer(stud, many=True)
        return Response(serializer.data)


class StudentsView(ListCreateAPIView):
    queryset = Student.objects.all()
    serializer_class = StudentSerializer
    permission_classes = [IsAuthenticated]


class StudentView(RetrieveUpdateDestroyAPIView):
    queryset = Student.objects.all()
    serializer_class = StudentSerializer
    permission_classes = [IsAuthenticated]


class ActivitiesView(ListCreateAPIView):
    queryset = Activity.objects.all()
    serializer_class = ActivitySerializer
    filter_backends = [filters.OrderingFilter, filters.SearchFilter]
    permission_classes = [IsAuthenticated]
    ordering_fields = ['name']  # localhost:8000/api/activyties?ordering=name
    # Exactly 'name'
    search_fields = ['name']  # /api/activyties?search=<activity_name>


class ActivityView(RetrieveUpdateDestroyAPIView):
    queryset = Activity.objects.all()
    serializer_class = ActivitySerializer
    permission_classes = [IsAuthenticated]


class AssessmentsView(ListCreateAPIView):
    queryset = Assessment.objects.all()
    serializer_class = AssessmentSerializer
    filter_backends = [filters.SearchFilter]
    permission_classes = [IsAuthenticated]
    search_fields = ['activity__name']  # /api/assessments?search=<activity_name>

    def get_serializer_class(self):
        return AssessmentCUDSerial if self.request.method == 'POST' else AssessmentSerializer


class AssessmentView(RetrieveUpdateDestroyAPIView):
    queryset = Assessment.objects.all()
    serializer_class = AssessmentSerializer
    permission_classes = [IsAuthenticated]

    def get_serializer_class(self):
        return AssessmentCUDSerial if self.request.method in ('PUT', 'PATCH') else AssessmentSerializer


class ReportsView(ListCreateAPIView):
    queryset = Report.objects.all()
    serializer_class = ReportSerializer
    permission_classes = [IsAuthenticated]


class ReportView(RetrieveDestroyAPIView):
    """
    Don't think we should have methods to update reports
    """
    queryset = Report.objects.all()
    serializer_class = ReportSerializer
    permission_classes = [IsAuthenticated]


class ExportDashboardView(GenericAPIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):
        time_periods = ('month', 'half_year')
        # /api/dashboard/export/?time_period= {month / half_year}
        time_period = self.request.query_params.get('time_period', None)

        # /api/dashboard/export/?date_start={%Y.%m.$d}&date_end={%Y.%m.$d}
        date_start = self.request.query_params.get('date_start', None)
        date_end = self.request.query_params.get('date_end', None)
        teacher_user = self.request.user
        if time_period is not None:
            if time_period in time_periods:
                send_xlsx_to_email.delay(teacher_user.id, time_period)
                return Response({
                    "result": "The exported file will be sent to you by e-mail soon"
                })
            else:
                return Response({
                    "result": f'Invalid date period value: time_period = {time_period}'
                })
        elif date_start is not None and date_end is not None:
            date_start = datetime.strptime(date_start, "%Y.%m.$d")
            date_end = datetime.strptime(date_end, "%Y.%m.$d")
            send_xlsx_to_email.delay(teacher_user.id, {
                "date_start": date_start,
                "date_end": date_end
            })
            return Response({
                "result": "The exported file will be sent to you by e-mail soon"
            })


class PerformanceView(ListAPIView):

    def get(self, request, class_id):
        response = {}
        queryset_class = get_object_or_404(Class, id=class_id)
        queryset_teacher = get_object_or_404(Teacher, id=queryset_class.creator_id)
        queryset_student = Student.objects.filter(class_am_id=class_id)
        list_students_id = list([student.id for student in queryset_student])
        queryset_assessment = Assessment.objects.filter(student__in=list_students_id)

        response['name'] = queryset_class.name
        response['teacher'] = queryset_teacher.email
        response['students'] = {}

        if self.request.query_params.get('date'):
            sampling_date = self.request.query_params.get('date')
            if datetime.datetime.strptime(sampling_date, '%d.%m.%Y'):
                sampling_date = datetime.datetime.strptime(sampling_date, '%d.%m.%Y')
                queryset_assessments = queryset_assessment.filter(
                    date__range=(sampling_date, datetime.datetime.today()))
                for student in queryset_student:
                    response['students'][student.name] = {}
                    for assessment in queryset_assessments:
                        if assessment.date.strftime("%d.%m.%y") in response['students'][student.name]:
                            response['students'][student.name][assessment.date.strftime("%d.%m.%y")].append({
                                assessment.activity.name: assessment.mark
                            })
                        else:
                            response['students'][student.name][assessment.date.strftime("%d.%m.%y")] = [{
                                assessment.activity.name: assessment.mark
                            }]
        else:
            sampling_date = datetime.datetime.today() - datetime.timedelta(days=180)
            queryset_assessments = queryset_assessment.filter(date__range=(sampling_date, datetime.datetime.today()))
            for student in queryset_student:
                response['students'][student.name] = {}
                for assessment in queryset_assessments:
                    if assessment.date in response['students'][student.name]:
                        response['students'][student.name][assessment.date.strftime("%d.%m.%y")].append({
                            assessment.activity.name: assessment.mark
                        })
                    else:
                        response['students'][student.name][assessment.date.strftime("%d.%m.%y")] = [{
                            assessment.activity.name: assessment.mark
                        }]

        return Response(response)


class PerformanceMonthView(ListAPIView):

    def get(self, request, class_id, month):
        response = {}
        queryset_class = get_object_or_404(Class, id=class_id)
        queryset_teacher = get_object_or_404(Teacher, id=queryset_class.creator_id)
        queryset_student = Student.objects.filter(class_am_id=class_id)
        list_students_id = list([student.id for student in queryset_student])
        queryset_assessment = Assessment.objects.filter(student__in=list_students_id)

        response['name'] = queryset_class.name
        response['teacher'] = queryset_teacher.email
        response['students'] = {}

        for student in queryset_student:
            additional_dict = {}
            assessment_list = []
            queryset_student_assessment = queryset_assessment.filter(student=student)
            for assessment in queryset_student_assessment:
                if assessment.date.month == month:
                    assessment_list.append(assessment.mark)
            if len(assessment_list) is not 0:
                average_mark = sum(assessment_list) / len(assessment_list)
                additional_dict['average'] = average_mark
            for assessment in queryset_student_assessment:
                if assessment.date.month == month:
                    if assessment.date in additional_dict:
                        additional_dict[assessment.date.strftime("%d.%m.%y")].append({
                            assessment.activity.name: assessment.mark
                        })
                    else:
                        additional_dict[assessment.date.strftime("%d.%m.%y")] = [{
                            assessment.activity.name: assessment.mark
                        }]
            response['students'][student.name] = additional_dict

        return Response(response)
