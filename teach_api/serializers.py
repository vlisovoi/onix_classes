from teach_admin.models import TeacherUser as Teacher, Class, Student, Activity, Assessment, Report
from rest_framework import serializers


"""
For service needs only  only serializers block. They aren't intended to be used outside this module 
"""
class MiniTeacherSerializer(serializers.ModelSerializer):
	class Meta:
		model = Teacher
		fields = ['first_name','last_name','email']

class MiniAssessmentSerializer(serializers.ModelSerializer):
	activity = serializers.CharField(read_only=True)

	class Meta:
		model = Assessment
		fields = ['activity','mark', 'date']


class MiniStudentSerializer(serializers.ModelSerializer):
	class_am = serializers.CharField(read_only=True)

	class Meta:
		model = Student
		fields = ['name','class_am']

"""
Service only serializers block end
"""



class StudentSerializer(serializers.ModelSerializer):
	assessments = MiniAssessmentSerializer(many=True,read_only=True)

	class Meta:
		model = Student
		fields = ['pk', 'name','class_am','assessments']




class ClassSerializer(serializers.ModelSerializer):
	students = StudentSerializer(many=True,read_only=True)
	creator = serializers.CharField(read_only=True)
	allowed_teachers = MiniTeacherSerializer(read_only=True,many=True)

	class Meta:
		model = Class
		fields = ['pk','name', 'creator', 'send_mail', 'allowed_teachers', 'students']

class ClassCUDSerial(serializers.ModelSerializer):
	creator = serializers.HiddenField(default=serializers.CurrentUserDefault())

	class Meta:
		model = Class
		fields = ['pk','name', 'creator', 'send_mail', 'allowed_teachers']




class ActivitySerializer(serializers.ModelSerializer):
	teacher = serializers.HiddenField(default=serializers.CurrentUserDefault())
	
	class Meta:
		model = Activity
		fields = ['pk','name', 'teacher' ]



class TeacherSerializer(serializers.ModelSerializer):
	classes = ClassSerializer(many=True, read_only=True)
	activities = ActivitySerializer(many=True, read_only=True)
	
	class Meta:
		model = Teacher
		fields = ['pk','email', 'password','first_name','last_name','email', 'avatar', 'classes', 'activities']
		extra_kwargs = {'password': {'write_only': True, 'required': True}}




class AssessmentSerializer(serializers.ModelSerializer):
	student = MiniStudentSerializer(many=True, read_only=True)
	activity = serializers.CharField(read_only=True)
	
	class Meta:
		model = Assessment
		fields = ['pk','activity', 'student', 'mark', 'date']


class AssessmentCUDSerial(serializers.ModelSerializer):

	class Meta:
		model = Assessment
		fields = ['activity', 'student', 'mark', 'date']




class ReportSerializer(serializers.ModelSerializer):
	creator = serializers.HiddenField(default=serializers.CurrentUserDefault())

	class Meta:
		model = Report
		fields = ['pk','title', 'report_type', 'creator', 'content']



class QuerySerializer(serializers.Serializer):
	student_name = serializers.CharField()