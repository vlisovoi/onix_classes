from django.urls import path
from teach_api.views import (
    TeachersView, TeacherView,
    ClassesView, ClassView,
    StudentsView, StudentView,
    ActivitiesView, ActivityView,
    AssessmentsView, AssessmentView,
    ReportsView, ReportView,
    SearchStudView, ExportDashboardView,
    PerformanceView, PerformanceMonthView)
from rest_framework_simplejwt import views as jwt_views

app_name = 'api'

urlpatterns = [
    path('teachers/', TeachersView.as_view(), name='teacher-list'),
    path('teachers/<int:pk>', TeacherView.as_view(), name='teacher-detail'),

    path('classes/', ClassesView.as_view(), name='class-list'),
    path('classes/<int:pk>/', ClassView.as_view(), name='class-detail'),
    path('classes/<int:pk>/<str:student_name>', SearchStudView.as_view(), name='search-stud'),

    path('students/', StudentsView.as_view(), name='student-list'),
    path('students/<int:pk>', StudentView.as_view(), name='student-detail'),

    path('activyties/', ActivitiesView.as_view(), name='activity-list'),
    path('activyties/<int:pk>', ActivityView.as_view(), name='activity-detail'),

    path('assessments/', AssessmentsView.as_view(), name='assessment-list'),
    path('assessments/<int:pk>', AssessmentView.as_view(), name='assessment-detail'),

    path('reports/', ReportsView.as_view(), name='report-list'),
    path('reports/<int:pk>', ReportView.as_view(), name='report-detail'),

    # jwt urls
    path('token/', jwt_views.TokenObtainPairView.as_view(), name='token-obtain-pair'),
    path('token/refresh/', jwt_views.TokenRefreshView.as_view(), name='token-refresh'),
    path('dashboard/export/', ExportDashboardView.as_view(), name='dashboard-export'),

    path('dashboard/<int:class_id>/', PerformanceView.as_view(), name='dashboard-detail'),
    path('dashboard/<int:class_id>/<int:month>', PerformanceMonthView.as_view(), name='dashboard-detail-month')
]
