from rest_framework import permissions


class ClassPermission(permissions.BasePermission):
    '''
    permission for get requests, granted only for users from add_user obj field
    other methods allowed only to creator
    '''

    def has_object_permission(self, request, view, obj):
        try:
            creator = request.user == obj.creator
        except AttributeError:
            creator = False
        else:
            if request.method == 'GET':
                allwd_teachers = request.user in obj.allowed_teachers.all()
                if creator or allwd_teachers:
                    return True  

        return creator
