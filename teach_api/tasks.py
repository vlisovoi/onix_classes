from teach_admin.models import Class, Student, Assessment, TeacherUser
from django.core.files.storage import default_storage
from dateutil.relativedelta import relativedelta
from django.core.mail import EmailMessage
from onix_class.celery import app
from datetime import datetime
import xlsxwriter, io, random, re
from onix_class.settings import MEDIA_ROOT
from django.shortcuts import get_object_or_404


@app.task
def send_xlsx_to_email(teacher_id, time_period):
    time_periods = ('month', 'half_year')
    queryset = Class.objects.all()
    queryset_students = Student.objects.all()
    queryset_assessment = Assessment.objects.all()
    teacher = get_object_or_404(TeacherUser, id=teacher_id)
    result = []
    queryset = queryset.filter(creator_id=teacher_id)
    list_classes_id = list([class_obj.id for class_obj in queryset])
    queryset_students = queryset_students.filter(class_am__in=list_classes_id)
    list_students_id = list([student_obj.id for student_obj in queryset_students])
    if time_period in time_periods:
        if time_period == "month":
            queryset_assessment = queryset_assessment.filter(
                student__in=list_students_id,
                date__range=(datetime.today() + relativedelta(months=-1), datetime.today())
            ).order_by("-date")
        elif time_period == "half_year":
            queryset_assessment = queryset_assessment.filter(
                student__in=list_students_id,
                date__range=(datetime.today() + relativedelta(months=-6), datetime.today())
            ).order_by("-date")
    elif type(time_period) is dict:
        queryset_assessment = queryset_assessment.filter(
            student__in=list_students_id,
            date__range=(time_period['date_start'], time_period['date_end'])
        ).order_by("-date")
    else:
        raise ValueError(f'Invalid date period value: time_period = {time_period}')
    first_line = list([assessment.date.strftime("%d.%m") for assessment in queryset_assessment])
    first_line = list(dict.fromkeys(first_line))
    first_line.insert(0, "Student / date")
    result.append(first_line)
    for student in queryset_students:
        assessments = queryset_assessment.filter(student=student)
        line = [f"{student.class_am.name} - {student.name}"]
        line += ["" for i in range(len(first_line) - 1)]

        for assessment in assessments:
            need_index = first_line.index(assessment.date.strftime("%d.%m"))
            line[need_index] += f"{assessment.activity.name} - {assessment.mark}"
        line = list([re.sub(r'\\n$', '', elem) for elem in line])
        result.append(line)
    xlsx_file = xlsxbuild(result, f"{teacher.first_name}_{teacher.last_name}_{datetime.now().strftime('%Y-%m-%d')}.xlsx")
    email = EmailMessage(
        'Export info',
        'Your request has been successfully exported.',
        'admin@onix-general.com',
        [teacher.email],
    )
    email.attach_file(f"{MEDIA_ROOT}/{xlsx_file}")
    email.send()


def xlsxbuild(data, filename):
    output = io.BytesIO()
    workbook = xlsxwriter.Workbook(output)
    worksheet = workbook.add_worksheet()
    wrap_format = workbook.add_format({'text_wrap': True})
    for row_num, columns in enumerate(data):
        for col_num, cell_data in enumerate(columns):
            worksheet.write(row_num, col_num, cell_data, wrap_format)

    workbook.close()
    output.seek(0)
    file = default_storage.save(filename, output)
    return file
