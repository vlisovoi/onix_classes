from django.apps import AppConfig


class TeachApiConfig(AppConfig):
    name = 'teach_api'
